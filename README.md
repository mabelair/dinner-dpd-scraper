# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* This script scrapes the Health Canada Drug Information Number "DPD" (Drug Product Database) system, and produces output in an easily readable table, and allows for the saving of results in a CSV for easy importation into R, SAS or other software.
* v0.1

### How do I get set up? ###

* Summary of set up
Upload to your shiny server as needed.
* Configuration
* Dependencies
Shiny Server, following R packages: shiny, httr, rvest, Hmisc.


### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Marc-André Bélair, Methodologist, Ottawa Hospital Research Institute: mbelair@ohri.ca